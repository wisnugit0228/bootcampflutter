//SOAL NO 1

// import 'dart:io';

// void main(List<String> args) {
//   print("Install Aplikasi? Y/T");
//   String? jawab = stdin.readLineSync()!;
//   var keluar = "$jawab";
//   keluar == "Y" ? print("anda akan menginstall aplikasi dart") : print("aborted");
// }

//SOAL NO 2
// import 'dart:io';

// void main(List<String> args) {
//   print("masukan nama");
//   String? nama = stdin.readLineSync()!;
//   var jawabNama = "$nama";
//   if (jawabNama != "") {
//     print("Selamat datang $nama Silahkan pilih peranmu");
//     print("pilih peran : penyihir, guard, warewolf");
//     String? peran = stdin.readLineSync()!;
//     var jawabPeran = "$peran";
//     if (jawabPeran != "") {
//       var pilihPeran = "$peran";
//       if (pilihPeran == "penyihir") {
//         print(
//             "Selamat datang di Dunia Werewolf, Halo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
//       } else if (pilihPeran == "guard") {
//         print(
//             "Selamat datang di Dunia Werewolf, $nama Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.");
//       } else if (pilihPeran == "warewolf") {
//         print(
//             "Selamat datang di Dunia Werewolf, $nama Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
//       } else {
//         print("Peran yang dipilih tidak tersedia");
//       }
//     } else {
//       print("kamu belum memasukan peran");
//     }
//   } else {
//     print("Nama harus diisi!");
//   }
// }

//SOAL NO 3

// import 'dart:io';

// void main(List<String> args) {
//   print("Masukan hari");
//   String? hari = stdin.readLineSync()!;

//   switch ("$hari") {
//     case "senin":
//       {
//         print(
//             'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
//         break;
//       }
//     case "selasa":
//       {
//         print(
//             'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
//         break;
//       }
//     case "rabu":
//       {
//         print(
//             'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
//         break;
//       }
//     case "kamis":
//       {
//         print(
//             'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
//         break;
//       }
//     case "jumat":
//       {
//         print('Hidup tak selamanya tentang pacar.');
//         break;
//       }
//     case "sabtu":
//       {
//         print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
//         break;
//       }
//     case "minggu":
//       {
//         print(
//             'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
//         break;
//       }
//     default:
//       {
//         print('masukan salah');
//       }
//   }
// }

// SOAL NO 4

// void main(List<String> args) {
//   var tanggal = '20';
//   int bulan = 3;
//   var tahun = '2001';

//   switch (bulan) {
//     case 1:
//       {
//         print(
//             '$tanggal Januari $tahun');
//         break;
//       }
//     case 2:
//       {
//         print(
//             '$tanggal Februari $tahun');
//         break;
//       }
//     case 3:
//       {
//         print(
//             '$tanggal Maret $tahun');
//         break;
//       }
//     case 4:
//       {
//         print(
//             '$tanggal April $tahun');
//         break;
//       }
//     case 5:
//       {
//         print('$tanggal Mei $tahun');
//         break;
//       }
//     case 6:
//       {
//         print('$tanggal Juni $tahun');
//         break;
//       }
//     case 7:
//       {
//         print(
//             '$tanggal Juli $tahun');
//         break;
//       }
//     case 8:
//       {
//         print(
//             '$tanggal Agustus $tahun');
//         break;
//       }
//     case 9:
//       {
//         print(
//             '$tanggal September $tahun');
//         break;
//       }
//     case 10:
//       {
//         print(
//             '$tanggal Oktober $tahun');
//         break;
//       }
//     case 11:
//       {
//         print(
//             '$tanggal November $tahun');
//         break;
//       }
//     case 12:
//       {
//         print(
//             '$tanggal Desember $tahun');
//         break;
//       }
//     default:
//       {
//         print('masukan salah');
//       }
//   }
// }
