//SOAL NO 1

// void main(List<String> args) {
//   print("Looping pertama");
//   int i = 2;
//   while (i <= 20) {
//     print(i.toString() + " - I LOVE CODING");
//     i++;
//   }

//   print("");
//   print("Looping kedua");
//   int j = 20;
//   while (j >= 2) {
//     print(j.toString() + " - I LOVE CODING");
//     j--;
//   }
// }

// SOAL NO 2

// void main(List<String> args) {
//   for (int i = 1; i <= 20; i++) {
//     if (i % 2 == 0) {
//       print(i.toString() + " - BERKUALITAS");
//     } else if (i % 3 == 0 && i % 2 != 0) {
//       print(i.toString() + " - I LOVE CODING");
//     } else {
//       print(i.toString() + " - SANTAI");
//     }
//   }
// }

//SOAL NO 3

// void main(List<String> args) {
//   int panjang = 8;
//   int lebar = 4;
//   for (int i = 1; i <= panjang; i++) {
//     for (int j = 1; j <= lebar; j++) {
//       print("# ");
//     }
//     print("");
//   }
// }

// SOAL NO 4

// void main(List<String> args) {
//   int tinggi = 7;
//   for (int i = 0; i < tinggi; i++) {
//     for (int j = 0; j <= i; j++) {
//       print("# ");
//     }
//     print("");
//   }
// }
